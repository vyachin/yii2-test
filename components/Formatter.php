<?php

namespace app\components;

use app\models\Call;
use app\models\Customer;
use app\models\Fax;
use app\models\History;
use Yii;

class Formatter extends \yii\i18n\Formatter
{
    /**
     * @param mixed $type
     *
     * @return mixed
     */
    public function getCustomerTypeTextByType($type)
    {
        return Customer::getTypeTexts()[$type] ?? $type;
    }

    /**
     * @param mixed $quality
     *
     * @return mixed
     */
    public function getCustomerQualityTextByQuality($quality)
    {
        return Customer::getQualityTexts()[$quality] ?? $quality;
    }

    /**
     * @return mixed|string
     */
    private function getCallFullDirectionText(int $value)
    {
        return Call::getFullDirectionTexts()[$value] ?? $value;
    }

    /**
     * @param Call $call
     *
     * @return mixed|string
     */
    public function getCallTotalStatusText($call)
    {
        if (
            $call->status == Call::STATUS_NO_ANSWERED
            && $call->direction == Call::DIRECTION_INCOMING
        ) {
            return Yii::t('app', 'Missed Call');
        }

        if (
            $call->status == Call::STATUS_NO_ANSWERED
            && $call->direction == Call::DIRECTION_OUTGOING
        ) {
            return Yii::t('app', 'Client No Answer');
        }

        $msg = $this->getCallFullDirectionText($call->direction);

        if ($call->duration) {
            $msg .= ' (' . $this->getCallDurationText($call) . ')';
        }

        return $msg;
    }

    /**
     * @param Call $call
     *
     * @return string
     */
    private function getCallDurationText($call)
    {
        if (!is_null($call->duration)) {
            return $call->duration >= 3600 ? gmdate("H:i:s", $call->duration) : gmdate("i:s", $call->duration);
        }
        return '00:00';
    }

    /**
     * @param mixed|string $value
     *
     * @return mixed|string
     */
    public function getFaxTypeText($value)
    {
        return Fax::getTypeTexts()[$value] ?? $value;
    }

    /**
     * @param string $event
     *
     * @return mixed
     */
    public static function asHistoryEventTextByEvent(string $event)
    {
        return History::getEventTexts()[$event] ?? $event;
    }

    public function getHistoryBodyByModel(History $model): string
    {
        switch ($model->event) {
            case History::EVENT_CREATED_TASK:
            case History::EVENT_COMPLETED_TASK:
            case History::EVENT_UPDATED_TASK:
                $task = $model->task;
                return $this->asHistoryEventTextByEvent($model->event) . ": " . ($task->title ?? '');
            case History::EVENT_INCOMING_SMS:
            case History::EVENT_OUTGOING_SMS:
                return $model->sms->message ? $model->sms->message : '';
            case History::EVENT_OUTGOING_FAX:
            case History::EVENT_INCOMING_FAX:
                return $this->asHistoryEventTextByEvent($model->event);
            case History::EVENT_CUSTOMER_CHANGE_TYPE:
                return $this->asHistoryEventTextByEvent($model->event) . " " .
                    ($this->getCustomerTypeTextByType(
                        $model->getDetailOldValue('type')
                    ) ?? "not set") . ' to ' .
                    ($this->getCustomerTypeTextByType($model->getDetailNewValue('type')) ?? "not set");
            case History::EVENT_CUSTOMER_CHANGE_QUALITY:
                return $this->asHistoryEventTextByEvent($model->event) . " " .
                    ($this->getCustomerQualityTextByQuality(
                        $model->getDetailOldValue('quality')
                    ) ?? "not set") . ' to ' .
                    ($this->getCustomerQualityTextByQuality(
                        $model->getDetailNewValue('quality')
                    ) ?? "not set");
            case History::EVENT_INCOMING_CALL:
            case History::EVENT_OUTGOING_CALL:
                $call = $model->call;
                return ($call ? $this->getCallTotalStatusText($call) : '<i>Deleted</i> ');
            default:
                return $this->asHistoryEventTextByEvent($model->event);
        }
    }
}
