<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=americor-mysql;dbname=americor-test',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => YII_DEBUG,
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',
];
