<?php

namespace {

    use app\components\Formatter;
    use yii\BaseYii;

    require __DIR__ . '/../vendor/yiisoft/yii2/BaseYii.php';

    /**
     * @property-read Formatter $formatter
     */
    trait BaseApplication
    {
    }

    class Yii extends BaseYii
    {
        /**
         * @var WebApplication|ConsoleApplication the application instance
         */
        public static $app;
    }

    class WebApplication extends yii\web\Application
    {
        use BaseApplication;
    }

    class ConsoleApplication extends yii\console\Application
    {
        use BaseApplication;
    }
}
