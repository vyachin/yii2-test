<?php

use yii\db\Migration;

/**
 * Class m191111_115918_init_sql
 *
 * @psalm-suppress UnusedClass
 */
class m191111_115918_init_sql extends Migration
{
    private $initTables = [
        'customer',
        'user',
        'history',
        'sms',

        'task',
        'call',
        'fax',
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp(): void
    {
        foreach ($this->initTables as $table) {
            foreach (file(__DIR__ . '/init/' . $table . '.sql') as $sql) {
                $this->execute($sql);
            }
        }
    }

    /**
     * @param string $sql
     * @param array $params
     *
     * @return void
     */
    public function execute($sql, $params = []): void
    {
        if (trim($sql)) {
            parent::execute($sql, $params);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): void
    {
        foreach (array_reverse($this->initTables) as $table) {
            $this->delete($table);
        }
    }
}

