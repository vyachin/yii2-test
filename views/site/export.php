<?php

/**
 * @var \yii\web\View $this
 * @var \app\models\History $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $exportType
 */

use app\models\History;
use app\widgets\Export\Export;

$filename = 'history';
$filename .= '-' . time();

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
?>

<?= Export::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'ins_ts',
            'label' => Yii::t('app', 'Date'),
            'format' => 'datetime',
        ],
        [
            'label' => Yii::t('app', 'User'),
            'value' => function (History $model) {
                return isset($model->user) ? $model->user->username : Yii::t('app', 'System');
            },
        ],
        [
            'label' => Yii::t('app', 'Type'),
            'attribute' => 'object',
        ],
        [
            'label' => Yii::t('app', 'Event'),
            'format' => 'historyEventTextByEvent',
            'attribute' => 'event',
        ],
        [
            'label' => Yii::t('app', 'Message'),
            'value' => function (History $model) {
                return strip_tags(Yii::$app->formatter->getHistoryBodyByModel($model));
            },
        ],
    ],
    'exportType' => $exportType,
    'batchSize' => 2000,
    'filename' => $filename,
]);