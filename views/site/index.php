<?php

use app\widgets\HistoryList\HistoryList;

/**
 * @var \yii\web\View $this
 */
$this->title = 'Americor Test';
?>

<div class="site-index">
    <?= HistoryList::widget() ?>
</div>
