<?php

use app\widgets\DateTime\DateTimeWidget;
use yii\helpers\Html;

/**
 * @var string $body
 * @var string $iconClass
 */
?>
<?= Html::tag('i', '', ['class' => "icon icon-circle icon-main white $iconClass"]); ?>

    <div class="bg-success ">
        <?= $body ?>

        <?php if (isset($bodyDatetime)): ?>
            <span><?= DateTimeWidget::widget(['dateTime' => $bodyDatetime]) ?></span>
        <?php endif; ?>
    </div>

<?php if (isset($user)): ?>
    <div class="bg-info"><?= Html::encode($user->username) ?></div>
<?php endif; ?>

<?php if (!empty($content)): ?>
    <div class="bg-info">
        <?= Html::encode($content) ?>
    </div>
<?php endif; ?>

<?php if (isset($footer) || isset($footerDatetime)): ?>
    <div class="bg-warning">
        <?= isset($footer) ? Html::encode($footer) : '' ?>
        <?php if (isset($footerDatetime)): ?>
            <span><?= DateTimeWidget::widget(['dateTime' => $footerDatetime]) ?></span>
        <?php endif; ?>
    </div>
<?php endif; ?>