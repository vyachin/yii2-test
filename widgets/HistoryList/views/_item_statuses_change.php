<?php

use app\models\History;
use app\widgets\DateTime\DateTimeWidget;
use yii\helpers\Html;

/**
 * @var History $model
 */
if (isset($oldValue)) {
    $oldValue = Html::encode($oldValue);
} else {
    $oldValue = "<i>not set</i>";
}
if (isset($newValue)) {
    $newValue = Html::encode($newValue);
} else {
    $newValue = "<i>not set</i>";
}
?>

    <div class="bg-success ">
        <?= Yii::$app->formatter->asHistoryEventTextByEvent($model->event) . " " .
        "<span class='badge badge-pill badge-warning'>{$oldValue}</span>" .
        " &#8594; " .
        "<span class='badge badge-pill badge-success'>{$newValue}</span>";
        ?>

        <span><?= DateTimeWidget::widget(['dateTime' => $model->ins_ts]) ?></span>
    </div>

<?php if (isset($model->user)): ?>
    <div class="bg-info"><?= Html::encode($model->user->username) ?></div>
<?php endif; ?>

<?php if (!empty($content)): ?>
    <div class="bg-info">
        <?= Html::encode($content) ?>
    </div>
<?php endif; ?>