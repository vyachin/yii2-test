<?php

namespace app\widgets\DateTime;

use Exception;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class DateTimeWidget extends Widget
{
    public $dateTime;

    /**
     * @return string
     * @throws Exception
     */
    public function run()
    {
        return
            Html::tag('i', '', ['class' => 'icon glyphicon glyphicon-time']) . ' ' .
            Yii::$app->formatter->format($this->dateTime, 'relativeTime') . ' - ' .
            Yii::$app->formatter->asDatetime($this->dateTime, 'MM/dd/y (hh:mm a)');
    }
}
